<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM TUGAS PHP</title>
</head>
<body>
    <h2>FORM TUGAS PHP</h2>

    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = $_POST["name"];
        $gender = $_POST["gender"];
        $city = $_POST["city"];


        echo "<h3>Form Data</h3>";
        echo "Name: " . $name . "<br>";
        echo "Jenis Kelamin : " . $gender . "<br>";
        echo "Kota Asal: " . $city . "<br>";
    } else {

    ?>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <label for="name">Nama:</label><br>
        <input type="text" id="name" name="name"><br>

        <label for="gender">Jenis Kelamin:</label><br>
        <input type="radio" id="male" name="gender" value="Laki - Laki ">
        <label for="male">Laki - Laki</label><br>
        <input type="radio" id="female" name="gender" value="Wanita">
        <label for="female">Wanita</label><br>

        <label for="city">Kota Asal:</label><br>
        <select id="city" name="city">
            <option value="Jakarta">Jakarta</option>
            <option value="Malang">Malang</option>
            <option value="Planet Bekasi">Planet Bekasi</option>
            <option value="Tangerang">Tangerang</option>
        </select><br>

        <input type="submit" value="Submit">
    </form>
    <?php } ?>
</body>
</html>
